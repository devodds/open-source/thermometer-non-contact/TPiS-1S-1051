EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED:CLS6B-FKW D1
U 1 1 5EF052CE
P 2650 3850
F 0 "D1" H 2650 4217 50  0000 C CNN
F 1 "CLS6B-FKW" H 2650 4126 50  0000 C CNN
F 2 "TPiS-1S-1051:TPiS-1S-1051_CLCC6_3.0x3.0mm_P1.2mm" H 2450 3530 50  0001 L CNN
F 3 "https://www.cree.com/led-components/media/documents/CLS6B-FKW.pdf" H 2450 3450 50  0001 L CNN
	1    2650 3850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
